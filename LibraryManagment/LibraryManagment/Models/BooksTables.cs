﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagment.Models
{
    [Table("BookInfo")]
    public class Book
    {
        [Key]
        public int Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Title { get; set; }
        public string Summary { get; set; }
        public int Price { get; set; }
        public string File { get; set; }
        public int Stock { get; set; }
        [Column(TypeName = "image")]
        public byte[] Image { get; set; }

        [ForeignKey("Category")]
        public int Category_Id { get; set; }
        public Category Category { get; set; }
        public short NumOfPages { get; set; }
        public short Weigth { get; set; }

        public string ISBN { get; set; }
        public Language Language { get; set; }

        public Discount Discount { get; set; }

        public List<Auther_Book> Auther_Books { get; set; }
        public List<Order_Book> Order_Books { get; set; }
        [ForeignKey("Publisher")]
        public int Publishe_Id { get; set; }
        public Publisher Publisher { get; set; }
        public List<Translator_Book> Translator_Books { get; set; }
    }
    public class Translator_Book
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Book")]
        public int Book_Id { get; set; }
        [ForeignKey("Translator")]
        public int Translator_Id { get; set; }

        public  Translator Translator { get; set; }
        public  Book Book { get; set; }


    }
    public class Translator
    {
        [Key]
        public int Id { get; set; }
        public string Translator_Name { get; set; }
        public string Translator_Family { get; set; }

        public List<Translator_Book> Translator_Books { get; set; }
    }
    public class Publisher
    {
        [Key]
        public int Id { get; set; }
        public string Publisher_Name { get; set; }
        public List<Book> Books { get; set; }
    }
    public class Auther_Book
    {
        [Key]
        public int Auther_Book_Id { get; set; }
      

        public Book Book { get; set; }

        public Auther Auther { get; set; }
    }
    public class Auther
    {
        [Key]
        public int Auther_Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public List<Auther_Book> Auther_Books { get; set; }
    }
    public class Discount
    {
        [Key, ForeignKey("Book")]
        public int Book_Id { get; set; }
        public DateTime Start_Date { get; set; }
        public DateTime? End_Date { get; set; }
        public byte Percent { get; set; }

        public Book Book { get; set; }
    }
    public class Language
    {
        [Key]
        public int Language_Id { get; set; }
        public int Language_Name { get; set; }
        public List<Book> Books { get; set; }
    }
    public class Category
    {
        [Key]
        public int Category_Id { get; set; }
        public string Name { get; set; }
  
        [ForeignKey("category")]
        public int? Parent_Id { get; set; }
        public Category category { get; set; }


    }

    //public class SubCategory
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public Category Category { get; set; }


    //    public List<Book> Books { get; set; }
    //}
    public class Customer
    {
        [Key]
        public int Customer_Id { get; set; }
        [Column("Fname", TypeName = "nvarchar(50)")]
        public string FirstName { get; set; }
        [Column("Lname"), MaxLength(100)]
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }
        public string Tell { get; set; }

        public int Age { get; set; }
        public DateTime Customer_BirthDate { get; set; }
        public City City { get; set; }
        public string Image { get; set; }
        public List<Orders> Orders { get; set; }


    }
    public class Order_Book
    {
        [Key]
        public int Id { get; set; }
        
        public Order_Book order { get; set; }
        [ForeignKey("Book")]
        public int Book_Id { get; set; }
        public Book Book { get; set; }
    }
    public class Orders
    {
        [Key]
        public string Order_Id { get; set; }
        public long Amount_Paid { get; set; }
        public string DispathNumber { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public DateTime Buy_Date { get; set; }
        public OrderStatus OrderStatus { get; set; }

        public List<Order_Book> Order_Books { get; set; }
    }
    public class OrderStatus
    {
        [Key]
        public int OrderStatus_Id { get; set; }
        public string OrderStatus_Name { get; set; }
        public List<Orders> Orders { get; set; }
    }
    public class Province
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None), Key]
        public int Province_ID { get; set; }
        public string Province_Name { get; set; }
        public List<City> Cities { get; set; }

    }
    public class City
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int City_ID { get; set; }
        public string City_Name { get; set; }

        public Province Province { get; set; }

        public List<Customer> Customers { get; set; }

    }
}
