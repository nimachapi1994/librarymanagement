﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagment.Models
{
    public class DB : DbContext
    {
        //protected override void OnConfiguring (DbContextOptionsBuilder builder)
        //{
        //    builder.UseSqlServer("data source=.;initial catalog=LibraryMiniProject;integrated security=True;MultipleActiveResultSets=True");
        //}
        public DB(DbContextOptions<DB> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //modelBuilder.Entity<Book>().Property(x => x.Image).HasColumnName("BookImage").HasColumnType("image");
            //modelBuilder.Entity<Book>().Property(x => x.Image).IsRequired();
            //modelBuilder.Entity<Book>().ToTable("BookInfo");
            //modelBuilder.Entity<Orders>().ToTable("PurchaseCart");
            //modelBuilder.Entity<Order_Book>().ToTable("PurchaseCart_Item");
            //modelBuilder.Entity<Book>().Property(x => x.Summary).HasMaxLength(200).IsRequired().HasColumnName("Book_Summary").HasColumnType("nvarchar(200)");

            //modelBuilder.Entity<Customer>().Property(x => x.FirstName).HasColumnType("nvarchar(50)").IsRequired()
            //    .HasMaxLength(50).HasColumnName("Fname");


            //modelBuilder.Entity<Customer>().Property(x => x.LastName).HasColumnType("nvarchar(50)").IsRequired()
            //    .HasMaxLength(50).HasColumnName("Lname");
            //modelBuilder.Entity<Customer>().Property(x => x.Image).IsRequired().HasColumnName("Profile_Image");
            //modelBuilder.Entity<Customer>().Ignore(x => x.GetCustomerImageFromClient);
            //modelBuilder.Entity<Category>().HasData(new Category
            //{
            //    Id = 1,
            //   Name="هنر" 
            //});



        }
        public DbSet<Book> Books { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Auther> Authers { get; set; }
        public DbSet<Auther_Book> Auther_Books { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Order_Book> Order_Books { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Discount> Discounts { get; set; }

    }
}
